<?php
// Start the session
session_start();

?>

<!DOCTYPE html>
<html>
<head>


  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/check_session.js"></script>
  <script src="/psdashboard/prod/js/news_fetch.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css" />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/dashboard_main.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css"  />

</head>
<body>


<div id="main" style="height:150px">
  <div id="header_nav" style="height:100px;" >
     <img height="100px" width="200px" src="/psdashboard/prod/imgs/DellEMC-Powered.png" style="margin-left:20px;"  />
      <div  align="center" style="float:right;height:100px;">
        <p style="display:inline-block; float:right;font-size:14px; color : #717171; align-items:center">
          <img alt="User Pic" id="user_img" src="<?php echo $_SESSION['dp'] ?>" class="img-circle img-responsive" height="60px" width="60px">
          <span> <?php echo $_SESSION['username'] ?> </span>
        </p>
      </div>
  </div>

  <nav class="main-menu" id = "navigation_bar">
  </nav>

  <br  />

  <div class="clear"> </div>
  <div id="spacer"> </div>
</div>

<div class = "outer_div" >
<div class = "content_div">


<div class="prod_row_dashboard container">

<div class="product_data" >
  <div style="width:105%; height:30px; background-color: #EFEFEF; display:block">

        <i class="fa fa-tasks" align="left" style="width :200px;"><span style="font-family:CustomBold; margin-left:10px; font-size:16px; "> Products Listing</span></i>

  </div>
  <div class="col-lg-3 prod_profile" >
      <a href="/psdashboard/prod/products/isilon/index.php"><img class = "prod_img" src="./imgs/isilon.jpg"  /></a>
      <br  />
      <h2 class="prod_heading" > Isilon </h2>

      <p class="prod_desc">
        Dell EMC Isilon scale-out NAS storage
        Store, manage, protect and analyze your unstructured data with the powerful platform that stays simple,  no matter how large your data environment
      </p>
    </div>

    <div class="col-lg-3 prod_profile" >
      <a href="/psdashboard/prod/products/datadobi/index.php"><img class = "prod_img" src="./imgs/datadobi.png"  /></a>
      <br  />
      <h2 class="prod_heading"> Data Dobi </h2>
      <p class="prod_desc">
        DobiMiner is purpose-built next-generation NAS and CAS migration software that helps businesses optimize their storage platform transition.

      </p>
    </div>

    <div class="col-lg-3 prod_profile" >
      <a href="#"><img class = "prod_img" src="./imgs/unity.jpg"  /></a>
      <br  />
      <h2 class="prod_heading"> Unity </h2>
      <p class="prod_desc">
        Unitys All-Flash and Hybrid Flash storage platforms optimize SSD performance and efficiency, with fully integrated SAN and NAS capabilities.
        Cloud-based storage analytics and proactive support keep you available and connected.

      </p>
    </div>

    <div class="col-lg-3 prod_profile" >
      <a href="#"><img class = "prod_img" src="./imgs/networker.png"  / style="height : 80px;"></a>
      <br  />
      <h2 class="prod_heading"> Networker </h2>
      <p class="prod_desc">
        EMC NetWorker is a suite of enterprise level data protection software that unifies and automates backup to tape,
        disk-based, and flash-based storage media across physical and virtual environments for granular and disaster recovery.

      </p>
    </div>





  </div>
</div>
<br  />

<div class=" prod_row_dashboard container latest_updates">
  <div style="width:105%; height:30px; background-color: #EFEFEF; display:block">

        <i class="fa fa-newspaper-o" align="left" style="width : 180px;">
          <span style="font-family:CustomBold; margin-left:10px; font-size:16px; ">
           Latest Updates</span></i>
           <div style="margin-left : 3%"  align="left">
             <br  />
           <table class="table table-hover">
             <thead>
               <tr>
                 <th align="center">Product</th>
                 <th align="center">Article Name</th>
                 <th align="center">Uploaded by</th>
                 <th align="center">Date of Upload</th>

               </tr>
             </thead>

             <tbody id = "load_news">

             </tbody>

           </table>
         </div>
           <div  style="right:0px;float:left">


           </div>

  </div>
</div>
<br  />
<div class=" prod_row_dashboard container top_contributers" >
  <div style="width:105%; height:30px; background-color: #EFEFEF; display:block">

        <i class="fa fa-users" align="left" style="width : 200px;"><span style="font-family:CustomBold; margin-left:10px; font-size:16px; ">
           Top Contributers</span></i>


  <div style="margin-left : 3%"  align="left">
  <br  />

  <table class="table table-hover">
    <thead>
      <tr>
        <th align="center">Name</th>
        <th align="center">Email Id</th>
        <th align="center">Profile Image</th>
        <th align="center">Total Contributions</th>
        <th align="center">Department</th>
      </tr>
    </thead>

    <tbody id = "contr_table_body">


    </tbody>
  </table>
</div>



  </div>

</div>

  <div class ="custom_footer">
    DELL - EMC &copy; 2017 <br />
    For Bug Report please contact : DL@EMC.COM
  </div>
</div>

</div>

</body>
</html>
