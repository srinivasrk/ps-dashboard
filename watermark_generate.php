<?php
function generate_watermark($filename)
{
require('rotation.php');

    class PDF extends PDF_Rotate{
          protected $_outerText1;// dynamic text
        protected $_outerText2;

        function setWaterText($txt1="", $txt2=""){
            $this->_outerText1 = $txt1;
            $this->_outerText2 = $txt2;
        }

        function Header(){
            //Put the watermark
            $this->SetFont('Arial','B',20);
            $this->SetTextColor(255,192,203);
            $this->SetAlpha(0.8);
            $this->RotatedText(15,190, $this->_outerText1, 45);
            $this->RotatedText(35,190, $this->_outerText2, 45);
        }

        function RotatedText($x, $y, $txt, $angle){
            //Text rotated around its origin
            $this->Rotate($angle,$x,$y);
            $this->Text($x,$y,$txt);
            $this->Rotate(0);
        }
    }

    $file = $filename;


    $pdf = new PDF();

    if (file_exists($file)){
        $pagecount = $pdf->setSourceFile($file) ;
    } else {
        
        return FALSE;
    }

   $pdf->setWaterText("Uploaded to Knowledge Management Tool ", "Check with document owner before using the document");
   $pdf->AddPage("P");
   $tpl = $pdf->importPage(1);
   $pdf->addPage();
   $pdf->useTemplate($tpl, 1, 1, 0, 0, TRUE);
  /* loop for multipage pdf */
   for($i=1; $i <= $pagecount ; $i++) {
     $tpl = $pdf->importPage($i);
     $pdf->addPage();
     $pdf->useTemplate($tpl, 1, 1, 0, 0, TRUE);
   }

    $updated_pdf = $pdf; //specify path filename to save or keep as it is to view in browser
    return $updated_pdf;
}
 ?>
