<?php
      session_start();
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "psdashboard";
      $num_rec_per_page=10;
      $output_string = "";

      if(isset($_POST['prodid']) && isset($_POST['pagenum'])){
        $prodname = $_POST['prodid'];
        $page = $_POST['pagenum'];
      }
      else{
        die("Unable to load the page");
      }
      $conn = mysqli_connect($servername, $username, $password, $dbname );
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
      $start_from = ($page-1) * $num_rec_per_page;
      $sql = "SELECT * FROM custom_scripts_tbl,employee_tbl where owner=empid AND status='approved' AND product = '".$prodname."' LIMIT $start_from, $num_rec_per_page";

      $rs_result = mysqli_query($conn, $sql); //run the query

      if(mysqli_num_rows($rs_result) > 0){

      while ($row = mysqli_fetch_assoc($rs_result)) {

        $tag_details = explode(",", $row['tags']);
        $tag_html = "<div style='font-family : CustomTag; position: relative; text-align:left; font-size : 16px;'>
        Tags :
        </div>";
        foreach($tag_details as $tempvar){
          $tag_html = $tag_html . "<span id = 'tags'>".$tempvar."</span>";
        }

        $cust_id = str_replace('"', "", $row['cust_id']);
        $prodname = str_replace('"', "", $prodname);

        $feedback_sql = "select * from feedback_like_tbl where  empid = '".$_SESSION['userID']."' and doc_type = 'script' and product_name = '".$prodname."' and doc_id = '".$cust_id."'";

        $feedback_result = mysqli_query($conn, $feedback_sql);
        $feedback_user_val = "-1";
        if (mysqli_num_rows($feedback_result) > 0) {
            while($row_feedback = mysqli_fetch_row($feedback_result)){
                $feedback_user_val =  $row_feedback[4];
            }
        }


          $output_string = $output_string . "
      <div class='row procedure_data'>
        <div class='col-lg-11 procedure_row'>
          <a onclick='updateViews(". ' "script" , ' . $cust_id . " )' class='procedure_heading' href=". $row['file_link'] . "
          <h4>".  $row['proc_heading'] ."</h4></a>
          <br  />


          <br  />
          <div class='procedure_desc'>
            <p >".  $row['proc_desc'] ."</p>

            <div class='tags_desc'>
            <p>".  $tag_html ."</p>
            </div>

          </div>




          <p class='procedure_owner'>
              Owner : ". $row['empname'] ."
          </p>

          <br  />

          <p class='procedure_date'>
              Total Views  : ".  $row['views_count'] ."
          </p>
          <br  />

          <p class='procedure_date'>
              Last Modified  : ".  $row['lastmodified_date'] ."
          </p>
          <br />
          <br />

          <br />";
          if($feedback_user_val == "-1")
          {
            $output_string = $output_string . "<button class='btn btn-danger' onclick='feedback(". ' "script" , "' . $prodname .  '", ' . $cust_id . " )' style='float:right;  '>
                No
          </button>

          <button class='btn btn-success' onclick='like_doc(". ' "script" , "' . $prodname .  '", ' . $cust_id . " )' style='float:right; margin-right : 5px;'>
                Yes
          </button>


          <p style='float:right; display:inline; padding : 5px; font-family : CustomBold' >
          Was this document helpful to you ?

          </p>

          <br  />";
        }

        else if ($feedback_user_val == "1"){

            $output_string = $output_string . '<img src = "/psdashboard/prod/imgs/like_img.png" style = "float : right; width : 30px; height : 30px;" />';


        }

        else{

            $output_string = $output_string . '<img src = "/psdashboard/prod/imgs/feedback_img.png" style = "float : right; width : 30px; height : 30px;" /> <span style="float:right; padding : 5px;font-family:CustomBold;"> Your Feedback is received </span> ';
        }


          $output_string = $output_string ." </div></div> <br />";

        }
      }
      else{
        $output_string = $output_string ."<div class='row procedure_data'>
          <div class='col-lg-11 procedure_row'>
          <h3 align='center'> No Documents to display</h3>
          </div></div>"
          ;
      }
      $sql = "SELECT * FROM custom_scripts_tbl where product = '".$prodname."' AND status='approved'";

      $rs_result = mysqli_query($conn, $sql); //run the query
      $total_records = mysqli_num_rows($rs_result);  //count number of records
      $total_pages = ceil($total_records / $num_rec_per_page);

      $output_string = $output_string . "<a style='margin-right:5px;' id = 'pagination' href='custom-scripts.php?page=1'> |< </a> "; // Goto 1st page

      for ($i=1; $i<=$total_pages; $i++) {
                $output_string = $output_string . "<a style='margin-right:5px;' id = 'pagination' href='custom-scripts.php?page=".$i."'>".$i."</a> ";
      }
      $output_string = $output_string . "<a id = 'pagination' href='custom-scripts.php?page=".$total_pages. "'> >|</a> ";

      echo $output_string;
?>
