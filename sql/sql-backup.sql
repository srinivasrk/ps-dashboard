-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for psdashboard
CREATE DATABASE IF NOT EXISTS `psdashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psdashboard`;


-- Dumping structure for table psdashboard.employee_tbl
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `empid` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `has_reg` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.employee_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `employee_tbl` DISABLE KEYS */;
REPLACE INTO `employee_tbl` (`empid`, `dob`, `email`, `has_reg`) VALUES
	('123456', '1992-10-23', 's@emc.com', 1),
	('158404', '1992-10-23', 'srinivas.kulkarni@emc.com', 1);
/*!40000 ALTER TABLE `employee_tbl` ENABLE KEYS */;


-- Dumping structure for table psdashboard.passwd_tbl
CREATE TABLE IF NOT EXISTS `passwd_tbl` (
  `empid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `reset_question` varchar(100) DEFAULT NULL,
  `reset_ans` varchar(200) DEFAULT NULL,
  UNIQUE KEY `Ntid` (`empid`),
  CONSTRAINT `FK_passwd_tbl_employee_tbl` FOREIGN KEY (`empid`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.passwd_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `passwd_tbl` DISABLE KEYS */;
REPLACE INTO `passwd_tbl` (`empid`, `password`, `reset_question`, `reset_ans`) VALUES
	('123456', 'Qwerty@123', NULL, NULL),
	('158404', 'Qwerty@123', NULL, NULL);
/*!40000 ALTER TABLE `passwd_tbl` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
