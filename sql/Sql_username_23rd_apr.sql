-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for psdashboard
CREATE DATABASE IF NOT EXISTS `psdashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psdashboard`;

-- Dumping structure for table psdashboard.custom_scripts_tbl
CREATE TABLE IF NOT EXISTS `custom_scripts_tbl` (
  `cust_count` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cust_count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.custom_scripts_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `custom_scripts_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_scripts_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.employee_tbl
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `empid` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `has_reg` int(11) NOT NULL DEFAULT '0',
  `empname` varchar(100) NOT NULL,
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.employee_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `employee_tbl` DISABLE KEYS */;
INSERT INTO `employee_tbl` (`empid`, `dob`, `email`, `has_reg`, `empname`) VALUES
	('123456', '1992-10-23', 's@emc.com', 1, 'IDK'),
	('158404', '1992-10-23', 'srinivas.kulkarni@emc.com', 1, 'srinivas');
/*!40000 ALTER TABLE `employee_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.kb_articles_tbl
CREATE TABLE IF NOT EXISTS `kb_articles_tbl` (
  `kb_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`kb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.kb_articles_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `kb_articles_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `kb_articles_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.learning_path_tbl
CREATE TABLE IF NOT EXISTS `learning_path_tbl` (
  `learning_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`learning_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.learning_path_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `learning_path_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_path_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.passwd_tbl
CREATE TABLE IF NOT EXISTS `passwd_tbl` (
  `empid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `reset_question` varchar(100) DEFAULT NULL,
  `reset_ans` varchar(200) DEFAULT NULL,
  UNIQUE KEY `Ntid` (`empid`),
  CONSTRAINT `FK_passwd_tbl_employee_tbl` FOREIGN KEY (`empid`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.passwd_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `passwd_tbl` DISABLE KEYS */;
INSERT INTO `passwd_tbl` (`empid`, `password`, `reset_question`, `reset_ans`) VALUES
	('123456', 'Qwerty@123', NULL, NULL),
	('158404', 'Qwerty@123', 'what was your first schools name?', 'Oxford');
/*!40000 ALTER TABLE `passwd_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.procedures_list
CREATE TABLE IF NOT EXISTS `procedures_list` (
  `proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  PRIMARY KEY (`proc_id`),
  KEY `owner_fk` (`owner`),
  KEY `product_fk` (`product`),
  CONSTRAINT `owner_fk` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`),
  CONSTRAINT `product_fk` FOREIGN KEY (`product`) REFERENCES `product_tbl` (`product_name`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.procedures_list: ~6 rows (approximately)
/*!40000 ALTER TABLE `procedures_list` DISABLE KEYS */;
INSERT INTO `procedures_list` (`proc_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`) VALUES
	(1, 'Isilon', 'GE Workaround', 'SmartConnect workaround configuration for GE Centricity PACS issue ... SmartConnect workaround configuration for GE Centricity PACS issue  .... SmartConnect workaround configuration for GE Centricity PACS issue', '158404', '2017-04-14 12:08:30', '/psdashboard/prod/uploads/isilon/GE-Worksround.pdf'),
	(44, 'Datadobi', 'Dobiminer Cutover Steps', '', '158404', '2017-04-14 17:10:30', '/psdashboard/prod/uploads/datadobi/Dobiminer Cutover steps.pdf'),
	(45, 'Isilon', 'Hadoop-demo part1 gphd-install', 'Part 1', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/hadoop-demo_part1_gphd-install.pdf'),
	(49, 'Isilon', 'Hadoop-demo part2', 'Part 2 - ISCSI configuration', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/hadoop-demo_part2_isiconfig.pdf'),
	(50, 'Isilon', 'Isilon ports requirement', '', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/Isilon ports requirement.pdf'),
	(51, 'Isilon', 'McKesson PACS - Official Isilon Sizing Guide _Rev13.pdf', 'Sizing guide for SA', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/McKesson PACS - Official Isilon Sizing Guide _Rev13.pdf');
/*!40000 ALTER TABLE `procedures_list` ENABLE KEYS */;

-- Dumping structure for table psdashboard.product_tbl
CREATE TABLE IF NOT EXISTS `product_tbl` (
  `product_name` varchar(100) NOT NULL,
  PRIMARY KEY (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.product_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `product_tbl` DISABLE KEYS */;
INSERT INTO `product_tbl` (`product_name`) VALUES
	('Datadobi'),
	('Isilon');
/*!40000 ALTER TABLE `product_tbl` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
