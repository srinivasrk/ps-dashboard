-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for psdashboard
CREATE DATABASE IF NOT EXISTS `psdashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psdashboard`;

-- Dumping structure for table psdashboard.custom_scripts_tbl
CREATE TABLE IF NOT EXISTS `custom_scripts_tbl` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL DEFAULT '0',
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.custom_scripts_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `custom_scripts_tbl` DISABLE KEYS */;
INSERT INTO `custom_scripts_tbl` (`cust_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`) VALUES
	(2, 'datadobi', 'test script', 'test', '158404', '2017-05-01 18:02:22', '/psdashboard/prod/uploads/isilon/158404/test%20script.pdf', 'approved', 2),
	(3, 'datadobi', 'test', 'test', '158404', '2017-04-02 01:40:24', '/psdashboard/prod/uploads/isilon/158404/test.pdf', 'refused', 0);
/*!40000 ALTER TABLE `custom_scripts_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.employee_tbl
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `empid` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `has_reg` int(11) NOT NULL DEFAULT '0',
  `empname` varchar(500) NOT NULL,
  `department` varchar(1000) NOT NULL DEFAULT 'Professional Services',
  `image_url` varchar(5000) NOT NULL DEFAULT 'Professional Services',
  `is_sme` varchar(20) NOT NULL DEFAULT 'non',
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.employee_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `employee_tbl` DISABLE KEYS */;
INSERT INTO `employee_tbl` (`empid`, `dob`, `email`, `has_reg`, `empname`, `department`, `image_url`, `is_sme`) VALUES
	('123456', '1992-10-23', 's@emc.com', 1, 'IDK', 'Professional Services', 'Professional Services', 'Isilon'),
	('158404', '1992-10-23', 'srinivas.kulkarni@emc.com', 1, 'Srinivas R Kulkarni', 'Professional Services', 'Professional Services', 'non');
/*!40000 ALTER TABLE `employee_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.kb_articles_tbl
CREATE TABLE IF NOT EXISTS `kb_articles_tbl` (
  `kb_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`kb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.kb_articles_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `kb_articles_tbl` DISABLE KEYS */;
INSERT INTO `kb_articles_tbl` (`kb_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`) VALUES
	(5, 'datadobi', 'Recoverpoint KB article', 'testing upload', '158404', '2017-04-28 19:26:10', '/psdashboard/prod/uploads/datadobi/158404/Recoverpoint%20KB%20article.pdf', 'refused', 3),
	(6, 'datadobi', 'test', 'test', '158404', '2017-05-03 02:00:13', '/psdashboard/prod/uploads/datadobi/158404/test.pdf', 'approved', 1);
/*!40000 ALTER TABLE `kb_articles_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.learning_path_tbl
CREATE TABLE IF NOT EXISTS `learning_path_tbl` (
  `learning_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`learning_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.learning_path_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `learning_path_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_path_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.passwd_tbl
CREATE TABLE IF NOT EXISTS `passwd_tbl` (
  `empid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `reset_question` varchar(100) DEFAULT NULL,
  `reset_ans` varchar(200) DEFAULT NULL,
  UNIQUE KEY `Ntid` (`empid`),
  CONSTRAINT `FK_passwd_tbl_employee_tbl` FOREIGN KEY (`empid`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.passwd_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `passwd_tbl` DISABLE KEYS */;
INSERT INTO `passwd_tbl` (`empid`, `password`, `reset_question`, `reset_ans`) VALUES
	('123456', 'Qwerty@123', NULL, NULL),
	('158404', 'Qwerty@123', 'what was your first schools name?', 'Oxford');
/*!40000 ALTER TABLE `passwd_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.procedures_list
CREATE TABLE IF NOT EXISTS `procedures_list` (
  `proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`proc_id`),
  KEY `owner_fk` (`owner`),
  KEY `product_fk` (`product`),
  CONSTRAINT `owner_fk` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`),
  CONSTRAINT `product_fk` FOREIGN KEY (`product`) REFERENCES `product_tbl` (`product_name`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.procedures_list: ~7 rows (approximately)
/*!40000 ALTER TABLE `procedures_list` DISABLE KEYS */;
INSERT INTO `procedures_list` (`proc_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`) VALUES
	(1, 'datadobi', 'GE Workaround', 'SmartConnect workaround configuration for GE Centricity PACS issue ... SmartConnect workaround configuration for GE Centricity PACS issue  .... SmartConnect workaround configuration for GE Centricity PACS issue', '158404', '2017-04-14 12:08:30', '/psdashboard/prod/uploads/isilon/158404/GE-Worksround.pdf', 'approved', 5),
	(45, 'isilon', 'Hadoop-demo part1 gphd-install', 'Part 1', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/hadoop-demo_part1_gphd-install.pdf', 'approved', 7),
	(49, 'isilon', 'Hadoop-demo part2', 'Part 2 - ISCSI configuration', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/hadoop-demo_part2_isiconfig.pdf', 'approved', 2),
	(50, 'isilon', 'Isilon ports requirement', '', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/Isilon%20ports%20requirement.pdf', 'approved', 1),
	(51, 'Isilon', 'McKesson PACS - Official Isilon Sizing Guide _Rev13.pdf', 'Sizing guide for SA', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/McKesson%20PACS%20-%20Official%20Isilon%20Sizing%20Guide%20_Rev13.pdf', 'approved', 1),
	(52, 'isilon', 'Test document', 'This is a demo', '158404', '2017-04-28 17:23:16', '/psdashboard/prod/uploads/isilon/158404/Test%20document.pdf', 'refused', 1),
	(53, 'isilon', 'testing123', 'test', '158404', '2017-05-03 17:08:09', '/psdashboard/prod/uploads/isilon/158404/testing123.pdf', 'approved', 2),
	(54, 'isilon', 'testingagain', 'test', '158404', '2017-05-04 01:41:48', '/psdashboard/prod/uploads/isilon/158404/testingagain.pdf', 'approved', 0),
	(55, 'isilon', 'newstest', 'hi there, i am testing', '158404', '2017-05-06 18:13:27', '/psdashboard/prod/uploads/isilon/158404/newstest.pdf', 'approved', 1);
/*!40000 ALTER TABLE `procedures_list` ENABLE KEYS */;

-- Dumping structure for table psdashboard.product_tbl
CREATE TABLE IF NOT EXISTS `product_tbl` (
  `product_name` varchar(100) NOT NULL,
  PRIMARY KEY (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.product_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `product_tbl` DISABLE KEYS */;
INSERT INTO `product_tbl` (`product_name`) VALUES
	('datadobi'),
	('isilon');
/*!40000 ALTER TABLE `product_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.video_tbl
CREATE TABLE IF NOT EXISTS `video_tbl` (
  `vid_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `vid_heading` varchar(5000) DEFAULT NULL,
  `vid_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`vid_id`),
  KEY `FK_vid_tbl_employee_tbl` (`owner`),
  KEY `FK_vid_tbl_product_tbl` (`product`),
  CONSTRAINT `FK_vid_tbl_employee_tbl` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_vid_tbl_product_tbl` FOREIGN KEY (`product`) REFERENCES `product_tbl` (`product_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.video_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `video_tbl` DISABLE KEYS */;
INSERT INTO `video_tbl` (`vid_id`, `product`, `vid_heading`, `vid_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`) VALUES
	(1, 'isilon', 'test', 'bruno', '158404', '2017-05-18 21:58:25', 'https://www.youtube.com/watch?v=J9pXeziqdLc', 'approved', 0);
/*!40000 ALTER TABLE `video_tbl` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
