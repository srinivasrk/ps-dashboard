-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for psdashboard
DROP DATABASE IF EXISTS `psdashboard`;
CREATE DATABASE IF NOT EXISTS `psdashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psdashboard`;

-- Dumping structure for table psdashboard.custom_scripts_tbl
DROP TABLE IF EXISTS `custom_scripts_tbl`;
CREATE TABLE IF NOT EXISTS `custom_scripts_tbl` (
  `cust_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL DEFAULT '0',
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(5000) NOT NULL,
  PRIMARY KEY (`cust_id`),
  KEY `owner_fk_1` (`owner`),
  CONSTRAINT `owner_fk_1` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.custom_scripts_tbl: ~6 rows (approximately)
/*!40000 ALTER TABLE `custom_scripts_tbl` DISABLE KEYS */;
INSERT INTO `custom_scripts_tbl` (`cust_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`, `tags`) VALUES
	(2, 'isilon', 'test script', 'test', '166980', '2017-04-28 18:02:22', '/psdashboard/prod/uploads/isilon/158404/test%20script.pdf', 'refused', 2, ''),
	(3, 'isilon', 'Test Custom Script Datadobi', 'Testing', '166980', '2017-05-02 23:42:17', '/psdashboard/prod/uploads/isilon/158404/Test%20Custom%20Script%20Datadobi.pdf', 'refused', 0, ''),
	(4, 'isilon', 'Test Isilon Custom Script', 'Testing', '158404', '2017-05-02 23:49:02', '/psdashboard/prod/uploads/isilon/158404/Test%20Isilon%20Custom%20Script.pdf', 'approved', 1, ''),
	(5, 'datadobi', 'Test news Upload', 'Testing document upload and news feed.', '158404', '2017-05-08 21:07:31', '/psdashboard/prod/uploads/datadobi/158404/Test%20news%20Upload.pdf', 'approved', 0, ''),
	(6, 'isilon', 'cust', 'ohhh', '158404', '2017-05-21 14:22:10', '/psdashboard/prod/uploads/isilon/158404/cust.pdf', 'approved', 0, 'as,asd,test'),
	(7, 'isilon', 'cust 2', 'fdeoi', '158404', '2017-05-21 14:22:37', '/psdashboard/prod/uploads/isilon/158404/cust%202.pdf', 'approved', 0, 'as,qwer');
/*!40000 ALTER TABLE `custom_scripts_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.employee_tbl
DROP TABLE IF EXISTS `employee_tbl`;
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `empid` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(100) NOT NULL,
  `has_reg` int(11) NOT NULL DEFAULT '0',
  `empname` varchar(500) NOT NULL,
  `department` varchar(1000) NOT NULL DEFAULT 'Professional Services',
  `image_url` varchar(5000) NOT NULL DEFAULT 'Professional Services',
  `is_sme` varchar(20) NOT NULL DEFAULT 'non',
  PRIMARY KEY (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.employee_tbl: ~3 rows (approximately)
/*!40000 ALTER TABLE `employee_tbl` DISABLE KEYS */;
INSERT INTO `employee_tbl` (`empid`, `dob`, `email`, `has_reg`, `empname`, `department`, `image_url`, `is_sme`) VALUES
	('123456', '1992-10-23', 'srinivas.kulkarni@emc.com', 1, 'Test User', 'Professional Services', '/psdashboard/prod/uploads/profile_pic/index.jpg', 'non'),
	('158404', '1992-10-23', 'srinivas.kulkarni@emc.com', 1, 'Srinivas R Kulkarni', 'Professional Services', '/psdashboard/prod/uploads/profile_pic/158404/index.jpg', 'Datadobi'),
	('166980', '1992-10-23', 's@emc.com', 1, 'Prateek Maurya', 'Professional Services', '/psdashboard/prod/uploads/profile_pic/index.jpg', 'Isilon');
/*!40000 ALTER TABLE `employee_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.kb_articles_tbl
DROP TABLE IF EXISTS `kb_articles_tbl`;
CREATE TABLE IF NOT EXISTS `kb_articles_tbl` (
  `kb_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(5000) NOT NULL,
  PRIMARY KEY (`kb_id`),
  KEY `owner_fk_2` (`owner`),
  CONSTRAINT `owner_fk_2` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.kb_articles_tbl: ~3 rows (approximately)
/*!40000 ALTER TABLE `kb_articles_tbl` DISABLE KEYS */;
INSERT INTO `kb_articles_tbl` (`kb_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`, `tags`) VALUES
	(5, 'isilion', 'Recoverpoint KB article', 'testing upload', '158404', '2017-05-10 19:26:10', '/psdashboard/prod/uploads/datadobi/158404/Recoverpoint%20KB%20article.pdf', 'approved', 4, 'as'),
	(6, 'isilon', 'test', 'afdasf', '158404', '2017-05-21 14:16:37', '/psdashboard/prod/uploads/isilon/158404/test.pdf', 'approved', 0, 'test,as'),
	(7, 'isilon', 'uw', 'wf', '158404', '2017-05-21 14:19:26', '/psdashboard/prod/uploads/isilon/158404/uw.pdf', 'approved', 0, 'as,test,asd');
/*!40000 ALTER TABLE `kb_articles_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.learning_path_tbl
DROP TABLE IF EXISTS `learning_path_tbl`;
CREATE TABLE IF NOT EXISTS `learning_path_tbl` (
  `learning_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) DEFAULT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`learning_id`),
  KEY `owner_fk_3` (`owner`),
  CONSTRAINT `owner_fk_3` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.learning_path_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `learning_path_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `learning_path_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.passwd_tbl
DROP TABLE IF EXISTS `passwd_tbl`;
CREATE TABLE IF NOT EXISTS `passwd_tbl` (
  `empid` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `reset_question` varchar(100) DEFAULT NULL,
  `reset_ans` varchar(200) DEFAULT NULL,
  UNIQUE KEY `Ntid` (`empid`),
  CONSTRAINT `FK_passwd_tbl_employee_tbl` FOREIGN KEY (`empid`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.passwd_tbl: ~3 rows (approximately)
/*!40000 ALTER TABLE `passwd_tbl` DISABLE KEYS */;
INSERT INTO `passwd_tbl` (`empid`, `password`, `reset_question`, `reset_ans`) VALUES
	('123456', 'Qwerty@123', 'In which state were you born ?', 'KAR'),
	('158404', 'Qwerty@123', 'what was your first schools name?', 'Oxford'),
	('166980', 'Qwerty@123', NULL, NULL);
/*!40000 ALTER TABLE `passwd_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.procedures_list
DROP TABLE IF EXISTS `procedures_list`;
CREATE TABLE IF NOT EXISTS `procedures_list` (
  `proc_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `proc_heading` varchar(5000) DEFAULT NULL,
  `proc_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `file_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(5000) NOT NULL,
  PRIMARY KEY (`proc_id`),
  KEY `owner_fk` (`owner`),
  KEY `product_fk` (`product`),
  CONSTRAINT `owner_fk` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`),
  CONSTRAINT `product_fk` FOREIGN KEY (`product`) REFERENCES `product_tbl` (`product_name`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.procedures_list: ~31 rows (approximately)
/*!40000 ALTER TABLE `procedures_list` DISABLE KEYS */;
INSERT INTO `procedures_list` (`proc_id`, `product`, `proc_heading`, `proc_desc`, `owner`, `lastmodified_date`, `file_link`, `status`, `views_count`, `tags`) VALUES
	(1, 'isilon', 'GE Workaround', 'SmartConnect workaround configuration for GE Centricity PACS issue ... SmartConnect workaround configuration for GE Centricity PACS issue  .... SmartConnect workaround configuration for GE Centricity PACS issue', '158404', '2017-05-11 12:08:30', '/psdashboard/prod/uploads/isilon/158404/GE-Worksround.pdf', 'approved', 14, 'as'),
	(45, 'isilon', 'Hadoop-demo part1 gphd-install', 'Part 1', '166980', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/hadoop-demo_part1_gphd-install.pdf', 'approved', 4, 'sad'),
	(49, 'isilon', 'Hadoop-demo part2', 'Part 2 - ISCSI configuration', '123456', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/hadoop-demo_part2_isiconfig.pdf', 'approved', 3, ''),
	(50, 'isilon', 'Isilon ports requirement', '', '166980', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/Isilon%20ports%20requirement.pdf', 'approved', 2, ''),
	(51, 'isilon', 'McKesson PACS - Official Isilon Sizing Guide _Rev13.pdf', 'Sizing guide for SA', '158404', '2017-04-14 12:10:30', '/psdashboard/prod/uploads/isilon/158404/McKesson%20PACS%20-%20Official%20Isilon%20Sizing%20Guide%20_Rev13.pdf', 'approved', 2, ''),
	(52, 'isilon', 'Test document', 'This is a demo', '158404', '2017-04-28 17:23:16', '/psdashboard/prod/uploads/isilon/158404/Test%20document.pdf', 'refused', 1, ''),
	(53, 'datadobi', 'Test document', 'This is a demo', '158404', '2017-04-28 17:23:16', '/psdashboard/prod/uploads/isilon/158404/Test%20document.pdf', 'approved', 1, ''),
	(54, 'datadobi', 'Test Datadobi upload', 'testing', '158404', '2017-05-02 23:44:29', '/psdashboard/prod/uploads/datadobi/158404/Test%20Datadobi%20upload.pdf', 'approved', 2, ''),
	(55, 'datadobi', 'May Upload Test', 'Testing upload', '158404', '2017-05-16 15:58:28', '/psdashboard/prod/uploads/datadobi/158404/May%20Upload%20Test.pdf', 'approved', 0, ''),
	(56, 'datadobi', 'Test Upload doc may 16', 'Test', '158404', '2017-05-16 16:39:27', '/psdashboard/prod/uploads/datadobi/158404/Test%20Upload%20doc%20may%2016.pdf', 'approved', 0, ''),
	(57, 'isilon', 'Test-watermark', 'Test', '158404', '2017-05-18 18:44:59', '/psdashboard/prod/uploads/isilon/158404/Test-watermark.pdf', 'pending', 0, ''),
	(58, 'isilon', 'test2', 'test2', '158404', '2017-05-18 18:46:51', '/psdashboard/prod/uploads/isilon/158404/test2.pdf', 'pending', 0, ''),
	(59, 'isilon', 'water test', 'water', '158404', '2017-05-18 18:50:01', '/psdashboard/prod/uploads/isilon/158404/water%20test.pdf', 'pending', 0, ''),
	(60, 'isilon', 'water test2', 'water test2', '158404', '2017-05-18 18:51:09', '/psdashboard/prod/uploads/isilon/158404/water%20test2.pdf', 'pending', 0, ''),
	(61, 'isilon', 'test3', 'test3', '158404', '2017-05-18 19:06:39', '/psdashboard/prod/uploads/isilon/158404/test3.pdf', 'pending', 0, ''),
	(62, 'isilon', 'test4', 'test4', '158404', '2017-05-18 19:08:01', '/psdashboard/prod/uploads/isilon/158404/test4.pdf', 'pending', 0, ''),
	(63, 'isilon', 'water', 'water', '158404', '2017-05-18 19:10:59', '/psdashboard/prod/uploads/isilon/158404/water.pdf', 'pending', 0, ''),
	(64, 'isilon', 'water2', 'water2', '158404', '2017-05-18 19:12:01', '/psdashboard/prod/uploads/isilon/158404/water2.pdf', 'pending', 0, ''),
	(65, 'isilon', 'water', 'water', '158404', '2017-05-18 19:15:31', '/psdashboard/prod/uploads/isilon/158404/water.pdf', 'pending', 0, ''),
	(66, 'isilon', 'water', 'water', '158404', '2017-05-18 19:16:39', '/psdashboard/prod/uploads/isilon/158404/water.pdf', 'pending', 0, ''),
	(67, 'isilon', 'water test', 'water test', '158404', '2017-05-19 12:49:52', '/psdashboard/prod/uploads/isilon/158404/water%20test.pdf', 'pending', 0, ''),
	(68, 'isilon', 'water test', 'water test', '158404', '2017-05-19 17:52:54', '/psdashboard/prod/uploads/isilon/158404/water%20test.pdf', 'pending', 0, ''),
	(69, 'isilon', 'water test', 'water test', '158404', '2017-05-19 20:07:46', '/psdashboard/prod/uploads/isilon/158404/water%20test.pdf', 'pending', 0, ''),
	(74, 'isilon', 'tag test', 'tag test', '158404', '2017-05-20 15:42:15', '/psdashboard/prod/uploads/isilon/158404/tag%20test.pdf', 'pending', 0, ''),
	(75, 'isilon', 'tag test', 'tag test', '158404', '2017-05-20 15:53:18', '/psdashboard/prod/uploads/isilon/158404/tag%20test.pdf', 'pending', 0, ''),
	(76, 'isilon', 'testwef', 'weofnu', '158404', '2017-05-20 21:11:55', '/psdashboard/prod/uploads/isilon/158404/testwef.pdf', 'approved', 0, 'test'),
	(77, 'isilon', 'qwerty', 'jytd', '158404', '2017-05-20 21:14:23', '/psdashboard/prod/uploads/isilon/158404/qwerty.pdf', 'approved', 0, 'sad,as'),
	(78, 'isilon', 'qq', 'asdsa', '158404', '2017-05-21 00:24:49', '/psdashboard/prod/uploads/isilon/158404/qq.pdf', 'pending', 0, 'a'),
	(79, 'isilon', 'asd', 'sdf', '158404', '2017-05-21 00:27:34', '/psdashboard/prod/uploads/isilon/158404/asd.pdf', 'approved', 1, 'as,sad'),
	(80, 'isilon', 'og', 'efewof', '158404', '2017-05-21 00:35:39', '/psdashboard/prod/uploads/isilon/158404/og.pdf', 'pending', 0, 'asds'),
	(81, 'isilon', 'qwe', 'sdfda', '158404', '2017-05-21 16:12:10', '/psdashboard/prod/uploads/isilon/158404/qwe.pdf', 'pending', 0, 'asd'),
	(82, 'isilon', 'sada', 'rewfr', '158404', '2017-05-21 16:53:15', '/psdashboard/prod/uploads/isilon/158404/sada.pdf', 'pending', 0, 'efd');
/*!40000 ALTER TABLE `procedures_list` ENABLE KEYS */;

-- Dumping structure for table psdashboard.product_tbl
DROP TABLE IF EXISTS `product_tbl`;
CREATE TABLE IF NOT EXISTS `product_tbl` (
  `product_name` varchar(100) NOT NULL,
  PRIMARY KEY (`product_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.product_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `product_tbl` DISABLE KEYS */;
INSERT INTO `product_tbl` (`product_name`) VALUES
	('datadobi'),
	('isilon');
/*!40000 ALTER TABLE `product_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.video_tbl
DROP TABLE IF EXISTS `video_tbl`;
CREATE TABLE IF NOT EXISTS `video_tbl` (
  `vid_id` int(11) NOT NULL AUTO_INCREMENT,
  `product` varchar(100) NOT NULL,
  `vid_heading` varchar(5000) DEFAULT NULL,
  `vid_desc` varchar(50000) DEFAULT NULL,
  `owner` varchar(50) DEFAULT NULL,
  `lastmodified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vid_link` varchar(5000) NOT NULL,
  `status` varchar(100) DEFAULT 'pending',
  `views_count` int(11) NOT NULL DEFAULT '0',
  `tags` varchar(5000) NOT NULL,
  PRIMARY KEY (`vid_id`),
  KEY `FK_video_tbl_employee_tbl` (`owner`),
  KEY `FK_video_tbl_product_tbl` (`product`),
  CONSTRAINT `FK_video_tbl_employee_tbl` FOREIGN KEY (`owner`) REFERENCES `employee_tbl` (`empid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_video_tbl_product_tbl` FOREIGN KEY (`product`) REFERENCES `product_tbl` (`product_name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.video_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `video_tbl` DISABLE KEYS */;
INSERT INTO `video_tbl` (`vid_id`, `product`, `vid_heading`, `vid_desc`, `owner`, `lastmodified_date`, `vid_link`, `status`, `views_count`, `tags`) VALUES
	(2, 'isilon', 'test', 'test', '158404', '2017-05-21 17:01:40', 'https://www.youtube.com/watch?v=OPf0YbXqDm0', 'approved', 0, 'test'),
	(3, 'datadobi', 'testing', 'iudwfbiuw', '158404', '2017-05-21 19:32:22', 'https://www.youtube.com/watch?v=09R8_2nJtjg', 'approved', 0, 'hi,there are');
/*!40000 ALTER TABLE `video_tbl` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
