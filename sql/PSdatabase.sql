-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for psdashboard
CREATE DATABASE IF NOT EXISTS `psdashboard` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `psdashboard`;

-- Dumping structure for table psdashboard.employee_tbl
CREATE TABLE IF NOT EXISTS `employee_tbl` (
  `Ntid` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `Email` varchar(100) NOT NULL,
  `HasReg` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Ntid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.employee_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `employee_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee_tbl` ENABLE KEYS */;

-- Dumping structure for table psdashboard.passwd_tbl
CREATE TABLE IF NOT EXISTS `passwd_tbl` (
  `Ntid` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Question` varchar(100) NOT NULL,
  `Answer` varchar(200) NOT NULL,
  UNIQUE KEY `Ntid` (`Ntid`),
  CONSTRAINT `FK_passwd_tbl_employee_tbl` FOREIGN KEY (`Ntid`) REFERENCES `employee_tbl` (`Ntid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table psdashboard.passwd_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `passwd_tbl` DISABLE KEYS */;
/*!40000 ALTER TABLE `passwd_tbl` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
