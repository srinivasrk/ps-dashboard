<?php
// Start the session
session_start();

?>

<!DOCTYPE html>
<html>
<head>
<title>Datadobi</title>

  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/check_session.js"></script>
  <script src="/psdashboard/prod/js/search_bar.js"></script>
  <script src="/psdashboard/prod/js/search_prod_filter.js"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css" />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/dashboard_main.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />

  
</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/psdashboard/prod/about.html">About</a>
  <a href="/psdashboard/prod/dashboard.html">Products</a>
  <a href="#">Contact Us</a>

</div>

<div id="main" class = "outer_div">
  <div id="header_nav" style="height:100px;" >
     <img height="100px" width="200px" src="/psdashboard/prod/imgs/DellEMC-Powered.png" style="margin-left:20px;"  />
      <div  align="center" style="float:right;height:100px;">
        <p style="display:inline-block; float:right;font-size:14px; color : #717171; align-items:center">
          <img alt="User Pic" src="/psdashboard/prod/uploads/profile_pic/158404/index.jpg" class="img-circle img-responsive" height="60px" width="60px">
           <span> <?php echo $_SESSION['username'] ?> </span>
        </p>
      </div>
  </div>


  <nav class="main-menu" id = "navigation_bar">

  </nav>
  <br  />
  <div align = "center" style="margin-bottom : 20px;">
      <img src = "/psdashboard/prod/imgs/datadobi.png"  style="width : 300px; height : 50px;" />
  </div>
        <div class="isilon_prod_main">

          <p class="isilon_prod_desc">
            DobiMiner is purpose-built next-generation NAS and CAS migration software that helps businesses optimize their storage platform transition.
            DobiMiner helps customers align all phases of a technology transition from an initial assessment through final monitoring and reporting.
          </p>

          <p class="disclaimer ">
            PS: All files uploaded here are best of the knowledge & are verified. Please check with the respective
            document owner before using them on a production environment
             Any issues with the document must be brought to the notice immediately and Knowledge Management Tool cannot be held responsible for any discrepancy
          </p>

        </div>

        <br  />




      <div class="row">
          <div class="col-xs-8 col-xs-offset-2">
          <div class="input-group">
                  <div class="input-group-btn search-panel">
                      <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                        <span id="search_concept">Filter by</span> <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" role="menu">
                        <li><a href="#title">Search By Title</a></li>
                        <li><a href="#owner">Search By Owner</a></li>
                        <li><a href="#tags">Search By Tags</a></li>
                      </ul>
                  </div>
                  <input type="hidden" name="search_param" value="all" id="search_param">
                  <input id="search" type="text" class="form-control" name="x" onkeypress="handleKeyPress(event)" placeholder="Search in datadobi...">
                  <span class="input-group-btn">
                      <button class="btn btn-default" type="button" onclick="search()"><span class="glyphicon glyphicon-search"></span></button>
                    <a href="#" onclick="reloadPage()">  <img src="/psdashboard/prod/imgs/clear_filter.png" width="75px" height="25px" style="margin:5px;margin-left:20px;"  /> </a>
                  </span>

              </div>
          </div>
    </div>



        <?php

            $server ="localhost";
            $username = "root";
            $password = "";
            $dbname = "psdashboard";

            $conn = mysqli_connect($server, $username, $password, $dbname);

            $sql = "select count(*) as count from procedures_list where product = 'datadobi' AND status='approved'";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $proc_count = $row['count'];

                }
            }

            $sql = "select count(*) as count from custom_scripts_tbl where product = 'datadobi' AND status='approved'";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $cus_count = $row['count'];

                }
            }


            $sql = "select count(*) as count from kb_articles_tbl where product = 'datadobi' AND status='approved'";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $kb_count = $row['count'];

                }
            }


            $sql = "select count(*) as count from learning_path_tbl where product = 'datadobi' AND status='approved'";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $learning_count = $row['count'];

                }
            }

            $sql = "select count(*) as count from video_tbl where product = 'datadobi' AND status='approved'";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $video_count = $row['count'];

                }
            }

         ?>
        <div class="row prod_row content_div" >

          <div class="col-lg-3 prod_profile" >
            <img class="procedures_icon" src="/psdashboard/prod/imgs/procedures.png" />
            <br  />
              <h1> <?php echo $proc_count ?> </h1>

            <a href="./procedures.php">  <h2 class="prod_heading"> Procedures </h2> </a>

              <p class="prod_desc">
                Procedure documents are specific to a certain activity. Procedure can contain a list of commands to execute or instructions to perform.
              </p>
            </div>

            <div class="col-lg-3 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/script_icon.png" />
              <br  />
              <h1> <?php echo $cus_count ?> </h1>
              <a href="./custom-scripts.php"> <h2 class="prod_heading"> Custom Scripts </h2></a>
              <p class="prod_desc">
                Custom Scripts are well defined codes which automates few tasks. These scripts make your work easier and more effecient. Please check with the script owner before using them.
              </p>
            </div>

            <div class="col-lg-3 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/knowledge_icon.png" />
              <br  />
              <h1> <?php echo $kb_count ?> </h1>
              <a href="./kb-articles.php"> <h2 class="prod_heading"> KB Articles </h2></a>
              <p class="prod_desc">
                List of popular KB - Articles for this product is listed here. These can be common issues / pitfalls or simply spcifications to understand more about the product.
              </p>
            </div>

            <div class="col-lg-3 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/learning_icon.png" />
              <br  />
              <h1> <?php echo $learning_count ?> </h1>
              <a href="#"> <h2 class="prod_heading"> Learning Path </h2></a>
              <p class="prod_desc">
                Just starting to learn this new product? Fear not, our SME's have made a list of Video ILT's and other useful documents which can make your learning curve smooth.
              </p>
            </div>

            <div class="col-lg-3 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/webex_icon.png" />
              <br  />
              <h1> <?php echo $video_count ?> </h1>
              <a href="./video.php"> <h2 class="prod_heading"> WebEx / Video Recordings </h2></a>
              <p class="prod_desc">
                Here are some of the useful Video's / WebEx recordings of implementations and useful video description of product features.
              </p>
            </div>


            <div id="data_row">
            </div>

        </div>

        <br  />




</div>

</body>
</html>
