<!DOCTYPE html>
<html>
<head>
  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/infinite-scroll.js"></script>
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css"  />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />

</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/psdashboard/prod/about.html">About</a>
  <a href="/psdashboard/prod/dashboard.html">Products</a>
  <a href="#">Contact Us</a>

</div>

<div id="main">
  <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776; Dell - EMC</span>
  <h2>Professional Service - Dashboard - XtremIO - Discussion</h2>
  <br  />
  <h1>Discussion Threads</h1>
    <br  />
		<ol id="posts">
			<li>
				<article>
					<header>
						<a href="#"><h4> XtremIO code 4.0.1 error in installing</h4> </a>
					</header>
          <p>
            Quisque facilisis aliquet dui, ut blandit odio fames ac turpis egestas.
          </p>
				</article>
			</li>
      <li>
				<article>
					<header>
						<a href="#"> <h4> XtremIO Migration with VPLEX</h4></a>
					</header>

          <p>
            Quisque facilisis aliquet dui, ut blandit odio vulputate et. Ut ac nisl turpis. Pellentesque scelerisque massa sit amet ipsum commodo cursus. Aenean eget ante et neque gravida tempor. Phasellus aliquam, purus quis malesuada vestibulum, sem m
          </p>
				</article>
			</li>
      <li>
				<article>
					<header>
						<a href="#"> <h4>XtremIO test host access</h4></a>
					</header>
          <p>
            Quisque facilisis aliquet dui, ut blandit odio vulputate et. Ut ac nisl turpis. Pellentesque scelerisque massa sit amet ipsum commodo cursus. Aenean eget ante et neque gravida tempor. Phasellus aliquam, p
          </p>
				</article>
			</li>

		</ol>
  </div>

</body>
</html>
