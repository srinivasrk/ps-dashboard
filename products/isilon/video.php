<?php
// Start the session
session_start();

?>

<!DOCTYPE html>
<html>
<head>

<title>Isilon Video</title>

  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/check_session.js"></script>
  <script src="/psdashboard/prod/js/search_bar.js"></script>
  <script src="/psdashboard/prod/js/load_default_video.js"></script>
  <script src="/psdashboard/prod/js/search_video_filter.js"></script>
    <script src = "/psdashboard/prod/js/like_feedback.js"></script>
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/custom_tags.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css" />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/dashboard_main.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />


</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/psdashboard/prod/about.html">About</a>
  <a href="/psdashboard/prod/dashboard.html">Products</a>
  <a href="#">Contact Us</a>

</div>

<div id="main">
  <div id="header_nav" style="height:100px;" >
     <img height="100px" width="200px" src="/psdashboard/prod/imgs/DellEMC-Powered.png" style="margin-left:20px;"  />
      <div  align="center" style="float:right;height:100px;">
        <p style="display:inline-block; float:right;font-size:14px; color : #717171; align-items:center">
          <img alt="User Pic" src="/psdashboard/prod/uploads/profile_pic/158404/index.jpg" class="img-circle img-responsive" height="60px" width="60px">
           <span> <?php echo $_SESSION['username'] ?> </span>
        </p>
      </div>
  </div>


  <nav class="main-menu" id = "navigation_bar">

  </nav>

        <div class="isilon_prod_main ">
          <div class="container" id ="filter_bar">
              <h1> Isilon - Procedures </h1>
              <p class="disclaimer">
                PS: All files uploaded here are best of the knowledge & are verified. Please check with the respective
                document owner before using them on a production environment
                 Any issues with the document must be brought to the notice immediately and Knowledge Management Tool cannot be held responsible for any discrepancy
              </p>
            <div class="row">
                <div class="col-xs-8 col-xs-offset-2">
        		    <div class="input-group">
                        <div class="input-group-btn search-panel">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            	<span id="search_concept">Filter by</span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <li><a href="#title">Search By Title</a></li>
                              <li><a href="#owner">Search By Owner</a></li>
                              <li><a href="#tags">Search By Tag</a></li>
                            </ul>
                        </div>
                        <input type="hidden" name="search_param" value="all" id="search_param">
                        <input id="search" type="text" class="form-control" name="x" onkeypress="handleKeyPress(event)" placeholder="Search...">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" onclick="search()"><span class="glyphicon glyphicon-search"></span></button>
                          <a href="#" onclick="reloadPage()">  <img src="/psdashboard/prod/imgs/clear_filter.png" width="75px" height="25px" style="margin:5px;margin-left:20px;"  /> </a>
                        </span>

                    </div>
                </div>
        	</div>
        </div>
          <div id="data_row">
          </div>
        </div>
</div>

</body>
</html>
