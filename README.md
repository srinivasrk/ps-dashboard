

# PS Dashboard / Knowledge Management Tool is a Content Management System (CMS) built out of HTML / CSS / Jquery / AJAX with backend PHP and MySQL database

## Working ##

The users have the ability to upload data (.pdf files / videos) into the portal which will trigger a notification to the SME (subject matter expert)
The SME will review the data and either approve / reject the data.

This process enables good quality and verified information to be uploaded into the CMS.

## Features ##

* Login and Registration page
* Upload of documents to backend server via PHP
* 2 step verification of document
* Adding custom watermark on PDF documents
* Feedback and like options on each document
* Notifications


## Screenshots ##

### Login Screen

![Dashboard](imgs/Login_screen.png)


### Homepage

![Homepage](imgs/homepage.png)


![Homepage](imgs/homepage2.png)


### Product homepage

![Dashboard](imgs/product_homepage.png)


### Procedures 

![Dashboard ](imgs/Procedures_page.png)


### Custom Watermark

![Dashboard](imgs/Watermark.png)

