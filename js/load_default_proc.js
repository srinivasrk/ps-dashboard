
var getParamsMap = function () {
    var params = window.location.search.split("&");

    var page_num = 1;
    params.forEach(function (p) {
        var v = p.split("=");

        page_num  = decodeURIComponent(v[1]);

    });
    return page_num;
};


$(document).ready(function () {

  var page_num = getParamsMap();
  if(page_num == 'undefined'){
    page_num = 1;
  }
  var page_name =  ($(document).find("title").text());

  if(page_name == "Datadobi Procedures"){
    page_name = "datadobi";
    $.ajax({
      type:'POST',
      url:"/psdashboard/prod/load_default_procedure.php",
      data:{prodid : page_name , pagenum : page_num},
      dataType:"html",
      cache:false,
      async:false,
      success:function(data){
          
          document.getElementById('data_row').innerHTML = data;
        }

     });
   }


  if(page_name == "Isilon Procedures"){

    page_name = "isilon";
    $.ajax({
      type:'POST',
      url:"/psdashboard/prod/load_default_procedure.php",
      data:{prodid : page_name , pagenum : page_num},
      dataType:"html",
      cache:false,
      async:false,
      success:function(data){

          document.getElementById('data_row').innerHTML = data;
        }
   });
  }
});
