$(document).ready(function () {
   $('#form-password, #form-password-confirm').keyup(function () {
   var $n = $('#form-password'),
       $c = $('#form-password-confirm'),
       newp = $n.val(),
       conf = $c.val();

   if(newp == '' ||  conf == ''){
     $('#form-password-confirm').css('background-color', 'white');
   }
   else if (  newp === conf) {
       $('#form-password-confirm').css('background-color', '#BFEE90');
       //$c.css('background-color', 'green');

   } else {
       $('#form-password-confirm').css('background-color', '#FF7373');

   }
  });
});

function registerUser(){
        var pass_txt = document.getElementById('form-password').value;
        var cpass_txt = document.getElementById('form-password-confirm').value;
        var empid_txt = document.getElementById('form-empid').value;
        var email_txt = document.getElementById('form-email').value;
        var dob_txt = document.getElementById('form-dob').value;
        var question_txt = document.getElementById('form-question').value;
        var answer_txt = document.getElementById('form-answer').value;
        var pic =  document.getElementById('form-pic').value;
        alert(pic);

        var empid_regex = /^[a-zA-Z0-9]{2,}$/ ;
        var email_regex = /^..*@..*$/;
        var password_regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
        var answer_regex = /^[a-zA-Z0-9 ]{2,}$/ ;

        if(!empid_regex.test(empid_txt))

            {
                alert("invalid format of Employee ID - only digits allowed");
                return false;

            }
        if(!email_regex.exec(email_txt))
            {
                alert("invalid format of email");
                return false;
            }
        if(!password_regex.exec(pass_txt))
            {
                alert("invalid formate of Password - at least on especial character, one capital letter, one digit and minimun 8 charaters");
                return false;
            }
        if(pass_txt != cpass_txt)
            {
                alert("please confirm the password again");
                return false;
            }
        if(!answer_regex.test(answer_txt))
            {
                alert("please enter at least 3 characters");
                return false;

            }
    $.ajax({
              type:'POST',
              url:"newuseraccount.php",
              data:{empid : empid_txt , pass : pass_txt, email : email_txt, dob : dob_txt, question : question_txt, answer : answer_txt,pic:pic},
              dataType:"html",
              cache:false,
              async:false,
              success:function(data){
                if(data.trim() == "1"){
                  alert("Successfully registered login using the sign in page")
                  location.href = "index.php"
                }
                else{
                  alert("Error occured while Registering the user. Please contact administrator");
                }
             }
            });
       return false;
      //setTimeout(function(){document.location.href = "confirmpassword.php"},500);
    }
