
/*

Created by Srinivas R Kulkarni

Use : This file will check if the session is running and if its not redirects to home page

*/

$(document).ready(function() {

$.ajax({

  type:'POST',
  url:"/psdashboard/prod/check_session.php",
  dataType:"html",
  cache:false,
  async:false,
  success:function(data){

    if(data.trim() == "1"){

    }
    else{
      alert("Session not found ! Please login again");
        setTimeout(function(){document.location.href = "/psdashboard/prod/index.php"},500);
    }
 }

});

$(document).ready(function () {

    $.ajax({
      type:'POST',
      url:"/psdashboard/prod/pendingcount.php",
      dataType:"html",
      cache:false,
      async:false,
      success:function(data){

        document.getElementById('navigation_bar').innerHTML = data;


      }
     });
});



$(document).ready(function () {

    $.ajax({
      type:'POST',
      url:"/psdashboard/prod/loadProfilePic.php",
      dataType:"html",
      cache:false,
      async:false,
      success:function(data){
          if(document.getElementById('user_img')){
            document.getElementById('user_img').src = data;
          }

      }
     });
});


$(document).ready(function () {

    $.ajax({
      type:'POST',
      url:"/psdashboard/prod/topContributers.php",
      dataType:"html",
      cache:false,
      async:false,
      success:function(data){
        if(document.getElementById('contr_table_body')){
          document.getElementById('contr_table_body').innerHTML = data;
        }

      }
     });
});

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.open($(this).data("href"), '_blank');
    });
});

});
