function approve(proc_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/proc_approve.php",
        data:{proc_id : proc_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_procedures.php"},500);
                return false;
       }
      });
}

function refuse(proc_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/proc_refuse.php",
        data:{proc_id : proc_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_procedures.php"},500);
                return false;
       }
      });
}
