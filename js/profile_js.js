jQuery(document).ready(function($) {
  $('#doc_type').change(function(){
    if($("#doc_type").val() == "videoWebEx") //I'm supposing the "Other" option value is 0.
        {

          $("#link").show();
          $("#upload_file").hide();
          $("#file_browse").hide();

      }
      });
});
jQuery(document).ready(function($) {
  $('#doc_type').change(function(){
    if($("#doc_type").val() != "videoWebEx") //I'm supposing the "Other" option value is 0.
          {

            $("#link").hide();
            $("#upload_file").show();
            $("#file_browse").show();
        }
});
});
//
jQuery(document).ready(function($) {
    $('#tags input').on('focusout',function(){
    var txt = $.trim( this.value );
    if(txt) {
      $(this).before('<span class="tag">'+txt+'</span>');
    }
    this.value = "";
  });
});

jQuery(document).ready(function($) {
  $('#tags').on('click','.tag',function(){
    if( confirm("Really delete this tag?") ) $(this).remove();
  });
});


function uploadFile(){

           var thefile = document.getElementById('upload_dialog').files[0].name;
           document.getElementById('upload_file').value= thefile;
       }

function copyFilesToServer(){

    var doc_title = document.getElementById('doc_title').value;
    var doc_desc = document.getElementById('doc_desc').value;
    var prod_name = document.getElementById('doc_prod').value;
    var doc_type = document.getElementById('doc_type').value;

    var myArray = $("#tags span").map(function() {
                 return $(this).text();
              }).get();

    if(myArray.length==0) {
      alert("tags cannot be empty");
      return false;
    }
    else
    {
    var tags = myArray.toString();
  }
    doc_title = doc_title.trim();
    doc_desc = doc_desc.trim();
    prod_name = prod_name.trim();
    doc_type = doc_type.trim();
    tags = tags.trim();
    if(doc_type=='videoWebEx'){
      var link = document.getElementById('video_webex_link').value;
      if(doc_title == '' || doc_desc == '' || prod_name == '' || doc_type == '' || tags == '' || link == ''){
        alert('please fill all the details before uploading');
      }
      else{
        $.ajax({
          type:'POST',
          url:"/psdashboard/prod/upload_link.php",
          data:{ doc_title : doc_title, doc_desc:doc_desc,prod_name:prod_name, doc_type:doc_type, tags:tags,link:link},
          dataType:"html",
          cache:false,
          async:false,
          success:function(data){
              alert(data);
              document.getElementById('doc_title').value = "";
              document.getElementById('doc_desc').value = "";
              document.getElementById('doc_prod').value = "";
              document.getElementById('doc_type').value = "";
              $("#tags span").remove();
              document.getElementById('video_webex_link').value = "";
            }


       });
      }
    }
    else {
    if(doc_title == '' || doc_desc == '' || prod_name == '' || doc_type == '' || tags == ''){
      alert('please fill all the details before uploading');
    }
    else{
      var form_data = new FormData();
      var upload_file_data = document.getElementById('upload_dialog').files[0];
      var upload_path = document.getElementById('upload_dialog').value;
      upload_path = upload_path.trim();
      if(upload_path != '')
      {
        form_data.append('upload_file', upload_file_data);
        form_data.append('doc_title',doc_title);
        form_data.append('doc_desc',doc_desc);
        form_data.append('prod_name',prod_name);
        form_data.append('doc_type',doc_type);
        form_data.append('tags',tags);



        $.ajax({
                  url: '/psdashboard/prod/upload_file.php', // point to server-side PHP script
                  dataType: 'text',  // what to expect back from the PHP script, if anything
                  cache: false,
                  contentType: false,
                  processData: false,
                  data: form_data,
                  type: 'post',
                  success: function(php_script_response){
                    alert(php_script_response);
                    document.getElementById('doc_title').value = "";
                    document.getElementById('doc_desc').value = "";
                    document.getElementById('doc_prod').value = "";
                    document.getElementById('doc_type').value = "";
                    $("#tags span").remove();
                    document.getElementById('upload_dialog').value = "";
                    document.getElementById('upload_file').value = "";
                  }
        });
    }
    else{
      alert("please upload the file before continuing");
    }
  }
}
}
