function search(){

    var search_txt = document.getElementById('search').value;
    search_txt = search_txt.trim();
    var filter_txt = document.getElementById('search_param').value;
    filter_txt = filter_txt.trim();

    var page_name =  ($(document).find("title").text());
    if(search_txt == "")
        {
            alert("search field cannot be left blank");
            return false;
        }
    if(filter_txt == "all")
        {
            alert("Please select a filter type");
            return false;
        }
    $.ajax({
          type:'POST',
          url:"/psdashboard/prod/search_script_filter.php",
          data:{search_txt : search_txt , filter_txt : filter_txt, page_name : page_name},
          dataType:"html",
          cache:false,
          async:false,
          success:function(data){
              document.getElementById('data_row').innerHTML = data;
          }
        });
 }
 function handleKeyPress(e){
 var key=e.keyCode || e.which;
  if (key==13){
     search();
  }
 }
