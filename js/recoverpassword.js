function forgot(){
/*

RULES FOR VARIABLE AND FUNCTION DECLARATION

  1. ALL INPUT FILEDS MUST HAVE _txt APPENDED

*/

    var empid_txt = document.getElementById('form-empid').value;
    var password_txt = document.getElementById('form-password').value;

    var empid_regex = /^[0-9]{2,}$/ ;
    //var password_regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;
    if(!empid_regex.test(empid_txt))
        {
            alert("invalid format of Employee ID - only digits allowed");
            return false;

        }
        // YOU DONT MENTION THE PASSWORD FORMAT HERE !!!!! ONLY CHECK FOR EMPID FORMAT !!!!!!!!!!!!!


  /*  if(!password_regex.exec(password))
        {
            alert("invalid format of Password - at least on especial character, one capital letter, one digit and minimun 8 charaters");
            return false;
        }*/
    $.ajax({
          type:'POST',
          url:"login_validate.php",
          data:{empid : empid_txt , pass : password_txt},
          dataType:"html",
          cache:false,
          async:false,
          success:function(data){

              if(data == "0"){
                  alert("you have not rigistered yet! try the sign up column");
                  setTimeout(function(){document.location.href = "index.php"},500);
                  return false;
            }
              else if(data == "1"){

                  setTimeout(function(){document.location.href = "dashboard.php"},500);
              }
              else{
                  alert("either your username or password is incorrect / contact administrator if problem persists");
                  return false;
              }
         }
        });
 }

function signup(){
    var empid_txt = document.getElementById('form-empid-reg').value;
    var dob_txt = document.getElementById('form-dob-reg').value;
    var email_txt = document.getElementById('form-email-reg').value;

    var empid_regex = /^[a-zA-Z0-9]{2,}$/ ;
    var email_regex = /^..*@..*$/;

    if(!empid_regex.test(empid_txt))
        {
            alert("invalid format of Employee ID - only digits allowed");
            return false;

        }
    if(!email_regex.exec(email_txt))
        {
            alert("invalid format of email");
            return false;
        }
   else{

     $.ajax({
           type:'POST',
           url:"verify_signup.php",
           data:{empid : empid_txt , email : email_txt, dob:dob_txt },
           dataType:"html",
           cache:false,
           async:false,
           success:function(data){

             if(data == "0"){
               alert("This user is already signed up. Try signing in / get your password reset");
               return;
             }
             else if(data == "-1"){
               alert("The user is not found in database, please contact administrator");
               return;
             }
             else{

               document.location.href = "confirmpassword.php?empid=" + empid_txt + "& dob="+ dob_txt +" & email="+email_txt ;
           }

          }
         });

   }

   return false;

}
