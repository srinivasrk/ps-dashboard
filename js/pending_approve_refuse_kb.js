function approve(kb_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/kb_approve.php",
        data:{kb_id : kb_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_kb-articles.php"},500);
                return false;
       }
      });
}

function refuse(kb_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/kb_refuse.php",
        data:{kb_id : kb_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_kb-articles.php"},500);
                return false;
       }
      });
}
