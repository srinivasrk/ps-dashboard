function approve(script_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/script_approve.php",
        data:{script_id : script_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_custom-scripts.php"},500);
                return false;
       }
      });
}

function refuse(script_id){
  $.ajax({
        type:'POST',
        url:"/psdashboard/prod/script_refuse.php",
        data:{script_id : script_id},
        dataType:"html",
        cache:false,
        async:false,
        success:function(data){
                alert(data);
                setTimeout(function(){document.location.href = "/psdashboard/prod/pending/pending_custom-scripts.php"},500);
                return false;
       }
      });
}
