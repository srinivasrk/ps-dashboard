

# PS Dashboard / Knowledge Management Tool is a Content Management System (CMS) built out of HTML / CSS / Jquery / AJAX with backend PHP and MySQL database

## Working ##

The users have the ability to upload data (.pdf files / videos) into the portal which will trigger a notification to the SME (subject matter expert)
The SME will review the data and either approve / reject the data.

This process enables good quality and verified information to be uploaded into the CMS.

## Screenshots ##

![login Screen](imgs/Login screen.png)