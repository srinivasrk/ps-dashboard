<?php
/*
select c.product, c.proc_heading, c.lastmodified_date, concat('custom-scripts.php') as articleid, e.empname
from custom_scripts_tbl c, employee_tbl e
where e.empid=c.owner AND c.`status`='approved'
AND c.lastmodified_date >= date('2017-05-01')
UNION
select c.product, c.proc_heading, c.lastmodified_date, concat('procedures.php') as articleid, e.empname
from procedures_list c, employee_tbl e
where e.empid=c.owner AND c.`status`='approved'
AND c.lastmodified_date >= date('2017-05-01')
UNION
select c.product, c.proc_heading, c.lastmodified_date, concat('kb-articles.php') as articleid, e.empname
from kb_articles_tbl c, employee_tbl e
where e.empid=c.owner AND c.`status`='approved'
AND c.lastmodified_date >= date('2017-05-01')
order by lastmodified_date desc
*/
      $servername = "localhost";
      $username = "root";
      $password = "";
      $dbname = "psdashboard";

      $flag=0;
      $conn = mysqli_connect($servername, $username, $password, $dbname );
      if (!$conn) {
          die("Connection failed: " . mysqli_connect_error());
      }
      $date = date('Y-m-d h:i:s', strtotime("-7 days"));
      $newsstring= " ";

        $sql = "select c.product, c.proc_heading, c.lastmodified_date, concat('custom-scripts.php') as articleid, e.empname
        from custom_scripts_tbl c, employee_tbl e
        where e.empid=c.owner AND c.`status`='approved'
        AND c.lastmodified_date >= date('".$date."')
        UNION
        select c.product, c.proc_heading, c.lastmodified_date, concat('procedures.php') as articleid, e.empname
        from procedures_list c, employee_tbl e
        where e.empid=c.owner AND c.`status`='approved'
        AND c.lastmodified_date >= date('".$date."')
        UNION
        select v.product, v.proc_heading, v.lastmodified_date, concat('video.php') as articleid, e.empname
        from video_tbl v, employee_tbl e
        where e.empid=v.owner AND v.`status`='approved'
        AND v.lastmodified_date >= date('".$date."')
        UNION
        select c.product, c.proc_heading, c.lastmodified_date, concat('kb-articles.php') as articleid, e.empname
        from kb_articles_tbl c, employee_tbl e
        where e.empid=c.owner AND c.`status`='approved'
        AND c.lastmodified_date >= date('".$date."')
        order by lastmodified_date desc";
    
        $rs_result = mysqli_query($conn, $sql); //run the query
        if(mysqli_num_rows($rs_result) > 0){
              $flag=1;
              while ($row = mysqli_fetch_assoc($rs_result)) {

                  $newsstring = $newsstring."<tr class='clickable-row' data-href='/psdashboard/prod/products/".$row['product']."/".$row['articleid']."' target = '_blank'>
                      <td>". strtoupper($row['product'])."</td>
                      <td>".$row['proc_heading']."</td>
                      <td> ".$row['empname']."</td>
                      <td>".$row['lastmodified_date']."</td>
                      </tr>";
              }
          }
        if($flag==0){
              $newsstring = $newsstring."
                  <tr><td></td><td><p  class=''>No updates from last week</p></td><td></td><td></td></tr>";
            }
    echo $newsstring;
?>
