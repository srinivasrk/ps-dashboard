<!DOCTYPE html>
<html lang="en">



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Confrim Registration </title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="login-assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="login-assets/font-awesome/css/font-awesome.min.css">
		    <link rel="stylesheet" href="login-assets/css/form-elements.css">
        <link rel="stylesheet" href="login-assets/css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="login-assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="login-assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="login-assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="login-assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="login-assets/ico/apple-touch-icon-57-precomposed.png">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="./js/get_password.js"></script>
    </head>

    <body>

      <!-- Navigation Bar codde -->
      <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand"  target="_blank" href="http://www.emc.com/en-us/index.htm">
                <img alt="Brand" src="./header-assets/img/mylogo.png" style="margin-bottom:20px;">
              </a>
            </div>

            <ul class="nav navbar-nav">
              <li><a href="./index.html"></a></li>
            </ul>
            <p class="navbar-text navbar-center" style="padding-right:20px;"></p>
             <div style="width=100%;float:right;position:absolute;right:0;top:0;padding:20px;">
              </div>
          </div>

      </nav>
      <!-- End of Navigation Bar -->
        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">


                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>  PS Dashboard </h1>
                            <div class="description">
                            	<p>
	                            	PS Dashboard is a central repository for employees to share knowledge on Dell EMC products
                            	</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
	                            </div>
	                            <div class="form-bottom">
				                    <form  class="login-form" onsubmit="return false">
				                    	<div class="form-group">
				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" disabled="true" name="form-username" placeholder="NTID..." class="form-ntid form-control" id="form-question" value="<?php echo $_GET['question']?>">
				                        </div>
				                        <div class="form-group">
				                    		<label class="sr-only" for="form-answer">Answer</label>
                                            <input type="text" name="form-answer" placeholder="Answer..." class="form-answer form-control" id="form-answer">
				                        </div>
                                        <button  onclick="forgot()" class="btn">Submit</button>
                                        <input type="hidden" id="form-empid" value="<?php echo $_GET['empid']?>">
				                    </form>
			                    </div>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>

        <!-- Footer
        <footer>
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				<p>Made by Anli Zaimi at <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a>
        					having a lot of fun. <i class="fa fa-smile-o"></i></p>
        			</div>
        		</div>
        	</div>
        </footer>-->
        <!-- Javascript -->
        <script src="login-assets/js/jquery-1.11.1.min.js"></script>
        <script src="login-assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="login-assets/js/scripts.js"></script>
        <!--[if lt IE 10]>
            <script src="login-assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
