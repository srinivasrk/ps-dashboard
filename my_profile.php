<?php
// Start the session
session_start();

?>

<!DOCTYPE html>
<html>
<head>


  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/check_session.js"></script>
  <script src="/psdashboard/prod/js/profile_js.js"></script>
  <script src="/psdashboard/prod/js/search_bar.js"></script>

  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/profile_css.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/tag.css" />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/dashboard_main.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css"  />

</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/psdashboard/prod/about.html">About</a>
  <a href="/psdashboard/prod/dashboard.html">Products</a>
  <a href="#">Contact Us</a>

</div>

<div id="main" class="outer_div">
  <div id="header_nav">
     <img height="100px" width="200px" src="/psdashboard/prod/imgs/DellEMC-Powered.png" style="margin-left:20px;"  />
    <p style="display:inline-block; float:right;padding:20px;font-size:14px; color : #717171">
       Logged in as <span> <?php echo $_SESSION['username'] ?> </span>
    </p>
  </div>

  <nav class="main-menu" id = "navigation_bar">

  </nav>

    <div class="container content_div">
      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad" >

<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "psdashboard";
$form_empid= $_SESSION['userID'];
$user="";
$dob="";
$depts="";
$email="";
$img = "";
$conn = mysqli_connect($servername, $username, $password, $dbname);
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
$sql = "SELECT is_sme, empname, image_url, department, dob, email FROM employee_tbl where empid = '".$form_empid."';"; // fetch all records with his/her name

        $exists = mysqli_query($conn, $sql);
        if ($exists->num_rows > 0) {
            while($row = mysqli_fetch_assoc($exists)){
            $user = $row['empname'];
            $smeof = $row['is_sme'];
            $img = $row['image_url'];
            $depts = $row['department'];
            $dob = $row['dob'];
            $email = $row['email'];
        }
      $_SESSION['username'] = $user;
      $_SESSION['userID'] = $form_empid;
      $_SESSION['smeof'] = $smeof;
      $_SESSION['img'] = $img;
      $_SESSION['dept'] = $depts;
      $_SESSION['dob'] = $dob;
      $_SESSION['email'] = $email;
}
?>
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title"><?php echo $_SESSION['username']  ?></h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center">
                  <img alt="User Pic" src= <?php echo $_SESSION['img']  ?> class="img-circle img-responsive">
                  <br  />
                  <a href="#"> Change Image</a>
                  <input type="file" id="upload_dialog"  accept=".pdf"  onchange="uploadFile()"   style="display: none;">
                  <label align="center" style="text-align:center; cursor:pointer; margin:5px;padding-left:15px;padding-right:15px;padding-top:5px;padding-bottom:5px;">
                    <img src="/psdashboard/prod/imgs/eidtmydp.png" style="width:25px;height:25px;">
                    <input type="file" id="upload_dialog"  accept=".png"  onchange="uploaddp()"   style="display: none;"></label>
                </div>


                <div class=" col-md-9 col-lg-9 ">
                  <br  />
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Department:</td>
                        <td><?php echo $_SESSION['dept']  ?></td>
                      </tr>

                      <tr>
                        <td>Date of Birth</td>
                        <td><?php echo $_SESSION['dob'] ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><?php echo $_SESSION['email']  ?></td>
                      </tr>
                        <td>Total Credit Points</td>
                        <td> <a href = "#"> Todo </a></td>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>


          </div>
        </div>




      </div>
      <div class="row upload_css" align="center">
        <div align="center">
            <h3>
              Upload your files to gain credits.
            </h3>
            <p class="text-center" style="padding : 20px;margin-left:20px; margin-right:20px;font-style:italic;">
              Uploading documents will alert the respective product SME's who will review the document for authenticity and correctness
              <br />Once the review is complete the document is uploaded on the portal.
            </p>
            <div class="row">
              <div class="col-lg-5 align_center" >
                <label  > Document Type  </label>
              </div>

              <div class="col-lg-6 text_box_css" >
                <div class="dropdown">
                  <select id ="doc_type" class="form-control">
                    <option id='procedure' value="procedure">Procedure</option>
                    <option id='script'value="script">Custom Script</option>
                    <option id='kbarticle' value="kbarticle">KB article</option>
                    <option id='videowebex' value="videoWebEx">video/WebEx link</option>
                  </select>
                </div>
              </div>

            </div>
        <div class="row">
          <div class="col-lg-5 align_center" >
            <label  > Document Title  </label>
          </div>

          <div class="col-lg-6 text_box_css" >
            <input id="doc_title" type="text" placeholder="Limit to 500 characters"  class="form-control"/>
          </div>

        </div>

        <div class="row">
          <div class="col-lg-5 align_center" >
            <label  > Document Description  </label>
          </div>

          <div class="col-lg-6 text_box_css" >
            <textarea id = "doc_desc" placeholder="Limit to 500 characters & Drag the text area to suit your typing"  class="form-control"></textarea>
          </div>

        </div>


        <div class="row">
          <div class="col-lg-5 align_center" >
            <label  > Select Product  </label>
          </div>

          <div class="col-lg-6 text_box_css" >
            <div class="dropdown">
              <select id = "doc_prod" class="form-control">
                <option value="isilon">Isilon</option>
                <option value="datadobi">DataDobi</option>

              </select>
            </div>

          </div>

        </div>





        <div class="row">
          <div class="col-lg-5 align_center" >
            <label  > Upload Document/video link </label>
          </div>

          <div class="col-lg-6 text_box_css" >
            <input type="text" id="upload_file"  class="form-control" disabled="true" />
          <div id='link' hidden>  <input type="text" id="video_webex_link"  class="form-control" /></div>
          </div>

          <div id="file_browse" class="col-lg-1" style="margin-top:12px;" >

            <label align="center" class="browse" style="text-align:center; cursor:pointer; margin:5px;padding-left:15px;padding-right:15px;padding-top:5px;padding-bottom:5px;">
              <img src="/psdashboard/prod/imgs/browse_icon.png" style="width:25px;height:25px;">
              <input type="file" id="upload_dialog"  accept=".pdf"  onchange="uploadFile()"   style="display: none;"></label>

          </div>
          <br  />
          <br  />
          <br  />
          </p>
        </div>
        <div class="row">
          <div class="col-lg-5 align_center" >
            <label  > Tags </label>

          </div>

          <div class="col-lg-6 text_box_css" >
            <div id="tags">
                      <input type="text" value="" placeholder="Add a tag" />
            </div>
          </div>


        </div>

            <br  />

            <button type="button" class="btn btn-primary  " style="margin-bottom: 20px;" onclick="copyFilesToServer()">Upload</button>
        </div>
        <p class="text-center" style="padding : 20px;margin-left:20px; margin-right:20px;font-style:italic;">
          Currently only PDF documents are allowed to be uploaded.
          <br  /> To convert your exisitng file to pdf download the <a href="/psdashboard/prod/dopdf-full.exe"> PDF Converter </a>
      </div>



    </div>

    <div class ="custom_footer">
      DELL - EMC &copy; 2017 <br />
      For Bug Report please contact : DL@EMC.COM
    </div>

</div>


</body>
</html>
