
<!--

TO DO : Create security question and answer boxes in the page.

-->
<?php

$empid = substr($_GET['empid'], -2);
$profilePic = "https://identity.corp.emc.com/EmpPhoto/".$empid."/".$_GET['empid']. ".jpg"

 ?>
<!DOCTYPE html>
<html lang="en">



    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Confrim Registration </title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="login-assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="login-assets/font-awesome/css/font-awesome.min.css">
		    <link rel="stylesheet" href="login-assets/css/form-elements.css">
        <link rel="stylesheet" href="login-assets/css/style.css">
        <link rel="stylesheet" href="/psdashboard/prod/css/cust-css.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="login-assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="login-assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="login-assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="login-assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="login-assets/ico/apple-touch-icon-57-precomposed.png">


        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
        <script src="./js/confirm_pass.js"></script>
    </head>

    <body>

      <!-- Navigation Bar codde -->
      <nav class="navbar navbar-inverse">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand"  target="_blank" href="http://www.emc.com/en-us/index.htm">
                <img alt="Brand" src="./header-assets/img/mylogo.png" style="margin-bottom:20px;">
              </a>
            </div>

            <ul class="nav navbar-nav">
              <li><a href="./index.html"></a></li>
            </ul>
            <p class="navbar-text navbar-center" style="padding-right:20px;"></p>
             <div style="width=100%;float:right;position:absolute;right:0;top:0;padding:20px;">
              </div>
          </div>

      </nav>
      <!-- End of Navigation Bar -->
        <!-- Top content -->
        <div class="top-content">

            <div class="inner-bg">


                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>  Knowledge Management tool </h1>
                            <div class="description">
                            	<p>
	                            	Knowledge Management tool is a central repository for employees to share knowledge on Dell EMC products
                            	</p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                        	<div class="form-box">
	                        	<div class="form-top">
	                        		<div class="form-top-left">
	                        			<h3> <strong> Confirm Password </strong></h3>
	                            		<p>Confrim your password to login to the site:</p>
	                        		</div>




	                        		<div class="form-top-right">
	                        			<i class="fa fa-key"></i>
	                        		</div>
                                <div style = "margin-left : 20px; margin-bottom : 10px; float : right; ">
	                            </div>

                              <img alt="User Pic" id="user_img" src="<?php echo $profilePic ?>" class="img-circle img-responsive" height="80px" width="160px">

                              </div>
	                            <div class="form-bottom">

				                    <form  class="login-form" onsubmit="return false">

				                    	<div class="form-group">

				                    		<label class="sr-only" for="form-username">Username</label>
				                        	<input type="text" disabled="true" name="form-username" placeholder="NTID..." class="form-ntid form-control" id="form-empid" value="<?php echo $_GET['empid']?>">
				                        </div>
				                        <div class="form-group">
				                        	<label class="sr-only" for="form-password">Password</label>
				                        	<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
				                        </div>

                                        <div class="form-group">
                                            <label class="sr-only" for="form-password">Confirm Password</label>
                                            <input type="password" name="form-password" placeholder="Confirm Password ..." class="form-password form-control" id="form-password-confirm">
                                        </div>
                                        <div class="form-group">
				                    		<label class="sr-only" for="form-question">Question</label>
                                            <select class="form-question form-control" id="form-question">
                                                <option value="what was your first schools name?">what was your first school's name?</option>
                                                <option value="what was your first cars model?">what was your first car's model?</option>

                                                <option value="What was your mother's maiden name ?">What was your mother's maiden name ?</option>
                                                <option value="In which state were you born ?">In which state were you born ?</option>
                                                <option value="Name of your first childhood pet">Name of your first childhood pet</option>
                                            </select>
				                        </div>
                                        <div class="form-group">
				                    		<label class="sr-only" for="form-answer">Answer</label>
                                            <input type="text" name="form-answer" placeholder="Answer to the selected question..." class="form-answer form-control" id="form-answer">
				                        </div>
                                        <input type="hidden" id="form-dob" value="<?php echo $_GET['dob']?>">
                                        <input type="hidden" id="form-email" value="<?php echo $_GET['email']?>">
                                        <input type="hidden" id="form-pic" value="<?php echo $profilePic ?>">
                                <button  onclick="registerUser()" class="btn">Submit</button>
				                    </form>
			                    </div>
		                    </div>

                        </div>

                    </div>

                </div>


        </div>

        <!-- Footer
        <footer>
        	<div class="container">
        		<div class="row">

        			<div class="col-sm-8 col-sm-offset-2">
        				<div class="footer-border"></div>
        				<p>Made by Anli Zaimi at <a href="http://azmind.com" target="_blank"><strong>AZMIND</strong></a>
        					having a lot of fun. <i class="fa fa-smile-o"></i></p>
        			</div>

        		</div>
        	</div>
        </footer>-->

        <!-- Javascript -->
        <script src="login-assets/js/jquery-1.11.1.min.js"></script>
        <script src="login-assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="login-assets/js/scripts.js"></script>

        <!--[if lt IE 10]>
            <script src="login-assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
