<?php


session_start();

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "psdashboard";

$conn = mysqli_connect($servername, $username, $password, $dbname );
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$arr = array("key" => "value");

$sql = "SELECT count(*) as count, owner from procedures_list where status = 'approved' group by owner limit 10;";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)){

          if(array_key_exists($row['owner'], $arr)) {
              $arr[$row['owner']] = $arr[$row['owner']] + $row['count'];

          }
          else {
            $arr[$row['owner']] = $row['count'] ;
          }

  }
}

$sql = "SELECT count(*) as count, owner from custom_scripts_tbl  where status = 'approved' group by owner limit 10;";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)){

          if(array_key_exists($row['owner'], $arr)) {
              $arr[$row['owner']] = $arr[$row['owner']] + $row['count'];

          }
          else {
            $arr[$row['owner']] = $row['count'] ;
          }

  }
}
$sql = "SELECT count(*) as count, owner from video_tbl where status = 'approved' group by owner limit 10;";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
      while($row = mysqli_fetch_assoc($result)){

          if(array_key_exists($row['owner'], $arr)) {
              $arr[$row['owner']] = $arr[$row['owner']] + $row['count'];

          }
          else {
            $arr[$row['owner']] = $row['count'] ;
          }

  }
}
$output = '';
$i = 0;
arsort($arr);

foreach ( array_keys($arr) as $keyval) {

  $sql = "SELECT empname, email, image_url, department from employee_tbl where empid = '".$keyval."';";
  $result = mysqli_query($conn, $sql);
  if (mysqli_num_rows($result) > 0 ) {
        while($row = mysqli_fetch_assoc($result)){
          $emailId = $row['email'];
          $image = $row['image_url'];
          $name = $row['empname'];
          $dept = $row['department'];
          $output = $output . '<tr>';
          $output = $output . '<td>'. $name . '</td>';
          $output = $output . '<td>'. $emailId . '</td>';
          $output = $output . '<td align="left"> <img alt="User Pic" id="user_img" src="'.$image.'"
          class="img-circle img-responsive" height="30px" width="30px">
           </td>';
           $output = $output . '<td>'. $arr[$keyval] . '</td>';
           $output = $output . '<td>'. $dept . '</td>';
           $output = $output . '</tr>';
          }

}
}

echo $output;


 ?>
