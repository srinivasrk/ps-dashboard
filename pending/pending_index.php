<?php
// Start the session
session_start();

?>

<!DOCTYPE html>
<html>
<head>


  <script src="/psdashboard/prod/jquery/jquery.min.js" ></script>
  <script src="/psdashboard/prod/js/sidenav.js" ></script>
  <script src="/psdashboard/prod/bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="/psdashboard/prod/js/check_session.js"></script>
  <?php if($_SESSION['smeof'] != "non"){
  echo "<script src='/psdashboard/prod/js/pending.js'></script>";
}
  ?>
    <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/custom_tags.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/sidenav.css" />
  <link rel="stylesheet" href="/psdashboard/prod/bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/dashboard_main.css" />
  <link rel="stylesheet" type="text/css" href="/psdashboard/prod/css/cust-css.css" />


</head>
<body>

<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="/psdashboard/prod/about.html">About</a>
  <a href="/psdashboard/prod/dashboard.html">Products</a>
  <a href="#">Contact Us</a>

</div>

<div id="main">
  <div id="header_nav" style="height:100px;" >
     <img height="100px" width="200px" src="/psdashboard/prod/imgs/DellEMC-Powered.png" style="margin-left:20px;"  />
      <div  align="center" style="float:right;height:100px;">
        <p style="display:inline-block; float:right;font-size:14px; color : #717171; align-items:center">
          <img alt="User Pic" src="/psdashboard/prod/uploads/profile_pic/158404/index.jpg" class="img-circle img-responsive" height="60px" width="60px">
           <span> <?php echo $_SESSION['username'] ?> </span>
        </p>
      </div>
  </div>


  <nav class="main-menu" id = "navigation_bar">

  </nav>

        <div class="isilon_prod_main">
          <h1> <?php  echo $_SESSION['smeof'];?> Pending Dashboard </h1>
          <p class="isilon_prod_desc">
            Dell EMC Isilon scale-out NAS storage
            Store, manage, protect and analyze your unstructured data with the powerful platform that stays simple, no matter how large your data environment.
          </p>
        </div>

        <?php

            $server ="localhost";
            $username = "root";
            $password = "";
            $dbname = "psdashboard";

            $conn = mysqli_connect($server, $username, $password, $dbname);

            $sql = "select count(*) as count from procedures_list where product = '".$_SESSION['smeof']."' AND status='pending' ;";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $proc_count = $row['count'];

                }
            }

            $sql = "select count(*) as count from custom_scripts_tbl where product  = '".$_SESSION['smeof']."' AND status='pending' ;";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $cus_count = $row['count'];

                }
            }


            $sql = "select count(*) as count from kb_articles_tbl where product  = '".$_SESSION['smeof']."' AND status='pending' ;";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $kb_count = $row['count'];

                }
            }
            $sql = "select count(*) as count from video_tbl where product  = '".$_SESSION['smeof']."' AND status='pending' ;";

            $result = mysqli_query($conn, $sql);

            if(mysqli_num_rows($result) > 0){
              while($row = mysqli_fetch_assoc($result)) {
                      $video_count = $row['count'];

                }
            }
         ?>
        <div class="row prod_row" style="margin-left : 15%" >

          <div class="col-lg-4 prod_profile" >
            <img class="procedures_icon" src="/psdashboard/prod/imgs/procedures.png" />
            <br  />
              <h1> <?php echo $proc_count ?> </h1>

            <a href="./pending_procedures.php">  <h2 class="prod_heading"> Procedures </h2> </a>

              <p class="prod_desc">
                Product description, If the images are different sizes you want to make sure they cover the div
                as well as being centered within it. Adding 50% border-radius will do the trick also.
              </p>
            </div>

            <div class="col-lg-4 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/script_icon.png" />
              <br  />
              <h1> <?php echo $cus_count ?> </h1>
              <a href="./pending_custom-scripts.php"> <h2 class="prod_heading"> Custom Scripts </h2></a>
              <p class="prod_desc">
                Product description, If the images are different sizes you want to make sure they cover the div
                as well as being centered within it. Adding 50% border-radius will do the trick also.
              </p>
            </div>

            <div class="col-lg-4 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/knowledge_icon.png" />
              <br  />
              <h1> <?php echo $kb_count ?> </h1>
              <a href="./pending_kb-articles.php"> <h2 class="prod_heading"> KB Articles </h2></a>
              <p class="prod_desc">
                Product description, If the images are different sizes you want to make sure they cover the div
                as well as being centered within it. Adding 50% border-radius will do the trick also.
              </p>
            </div>
            <div class="col-lg-3 prod_profile" >
              <img class="procedures_icon" src="/psdashboard/prod/imgs/webex_icon.png" />
              <br  />
              <h1> <?php echo $video_count ?> </h1>
              <a href="./pending_video.php"> <h2 class="prod_heading"> WebEx / Video Recordings </h2></a>
              <p class="prod_desc">
                Product description, If the images are different sizes you want to make sure they cover the div
                as well as being centered within it.
              </p>
            </div>
        </div>



</div>

</body>
</html>
