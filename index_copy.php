<?php
// Start the session
session_start();
session_unset();
session_destroy();

?>
<!DOCTYPE html>
<html lang="en">
<!-- TO DO :

  1. Edit /prod/js/cust-js.js to validate sign up and sign in
  2. If user is present is data base check the field ( is registered (Y/N)) if Y then login
  3. If user is not registered ask them to register first and take validate the input and get a password
  4. login and redirect to dashbaord page and display the user who is logged in.
  5. keep the session between the pages and have a logout button.


-->

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Login &amp; Register</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="login-assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="login-assets/font-awesome/css/font-awesome.min.css">
		    <link rel="stylesheet" href="/psdashboard/prod/css/login-css.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="login-assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="login-assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="login-assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="login-assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="login-assets/ico/apple-touch-icon-57-precomposed.png">


        <script src="./js/login-script.js"></script>

    </head>

    <body>

    <div class="heading">
        K. M. T.
    </div>

  <div class="description_index">
      knowledge Management tool
  </div>

  <div class="more_desc">
    A Central repository for knowledge management. <br />
Share procedures, Articles, Custom Scripts , WebEx recordings and much more ...
  </div>

    <div class="login_button">
      <p class="Login" > Login </p>
    </div>

    </body>

  </html>
